package model.data_structures;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

//
////import java.util.ArrayList;  
////import java.util.Iterator;
//
public class RingList<T> implements Collection<T> {


	private final ArrayList<T> lista;
	private final int tamanioMaximo;

	public RingList(int pTamanioMaximo)
	{
		this.lista = new ArrayList<T>(pTamanioMaximo);
		this.tamanioMaximo = pTamanioMaximo;
	}

	@Override
	public boolean add(T elem)
	{
		if(lista.size() == tamanioMaximo)
			lista.remove(0);

		return lista.add(elem);

	}

	@Override
	public boolean addAll(Collection<? extends T> c) 
	{

		boolean agrego = false;

		for(T elem : c)
			agrego = this.add(elem) | agrego;

		return agrego;
	}

	@Override
	public void clear()
	{
		lista.clear();
	}

	@Override
	public boolean contains(Object o) 
	{
		return lista.contains(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) 
	{
		return lista.containsAll(c);
	}

	@Override
	public boolean isEmpty() {
		return lista.isEmpty();
	}

	@Override
	public Iterator<T> iterator() {
		return lista.iterator();
	}

	@Override
	public boolean remove(Object o) {
		return lista.remove(o);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return lista.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return lista.retainAll(c);
	}

	@Override
	public int size() {
		return lista.size();
	}

	@Override
	public Object[] toArray() 
	{
		return lista.toArray();
	}


	@Override
	public <T> T[] toArray(T[] a) {
		return lista.toArray(a);
	}


	
	public T getByPosicion(int posicion)
	{
		return lista.get(posicion);
	}
	
	public T getPrimero()
	{
		T primero = null;
		
		if(!lista.isEmpty())
			primero = lista.get(0);
		
		return primero;
					
	}
	
	public T getUltimo()
	{
		T ultimo = null;
		
		if(!lista.isEmpty())
			ultimo = lista.get(lista.size()-1);
		
		return ultimo;
	}
}