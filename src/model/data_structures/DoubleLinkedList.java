package model.data_structures;

public class DoubleLinkedList <T>{



  // Clase auxiliar Node

  
public class Node {

    Node next;

    Node previous;

    T data;



    public Node(Node next, T data) {

      this.next = next;

      this.data = data;

    }



    public Node(T elem) {

      this(null, elem);

    }

  }



  private Node primero;

  private Node posicionActual;



  /**

   * Incrementa la posición inicial en las posiciínes obtenidas como paramtero

   */

  public void siguiente(int numPosiciones) {

    if (numPosiciones > 0 && primero != null) {

      int posiciones = numPosiciones;

      if (posicionActual == null) {

        posicionActual = primero;

        posiciones--;

      }

      while (posicionActual.next != null && posiciones > 0) {

        posicionActual = posicionActual.next;

        posiciones--;

      }

    }

  }



  /**

   * Inserta un objeto en la posición actual.

   */

  public void insert(T data) {

    Node node = new Node(data);



    if (posicionActual == null) {

      // inserts in first node

      node.next = primero;

      if (primero != null) {

        primero.previous = node;

      }

      primero = node;

    } else {

      // the node isn't the first.

      node.next = posicionActual.next;

      node.previous = posicionActual;

      if (posicionActual.next != null) {

    	  posicionActual.next.previous = node;

      }

      posicionActual.next = node;

    }



    // updates current position

    posicionActual = node;

  }



  /**

   * Borra el nodo referenciado por la posición actual.

   * 

   * @return l objeto a borrar

   */

  public T borrar() {

    T data = null;



    if (posicionActual != null) {

      data = posicionActual.data;



      // 'borra' el nodo

      if (primero == posicionActual) {

        primero = posicionActual.next;

      } else {

    	  posicionActual.previous.next = posicionActual.next;

      }



      if (posicionActual.next != null) {

    	  posicionActual.next.previous = posicionActual.previous;

      }



      // next position

      posicionActual = posicionActual.next;

    }

    return data;

  }



  /**

   * Go back -numPositions-

   */

  public void volver(int numPositions) {

    if (numPositions <= 0 || primero == null || posicionActual == null)

      return;

    int positionsBack = numPositions;

    while (posicionActual != null && positionsBack > 0) {

    	posicionActual = posicionActual.previous;

      positionsBack--;

    }
  }
}
  

